package ru.t1.malyugin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-complete-by-index";

    @NotNull
    private static final String DESCRIPTION = "Complete project by index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextInteger() - 1;
        @NotNull final String userId = getUserId();
        getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
    }

}